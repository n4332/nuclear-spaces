# Nuclear Spaces Presentation

The .tex file in this repository is the writeup for a short 25 minute presentation as part of a Functional Analysis course at CU Boulder, Fall 2021. As always, any corrections and comments are appreciated in the form of a pull request.

A pdf of the current LaTeX code is available [here](https://gitlab.com/n4332/nuclear-spaces/-/jobs/artifacts/main/raw/nuclear-spaces.pdf?job=compile-tex).
