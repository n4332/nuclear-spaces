\documentclass[letterpaper, 12pt]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{textcomp}
\usepackage[english]{babel}
\usepackage{amsmath}
\usepackage{amssymb, amsthm}
%\usepackage{xcolor}
\usepackage{hyperref}

\newtheorem{theorem}{Theorem}[section]
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{claim}[theorem]{Claim}
\theoremstyle{remark}
\newtheorem{remark}[theorem]{Remark}
\theoremstyle{definition}
\newtheorem{definition}[theorem]{Definition}


\newcommand{\N}{\ensuremath{\mathbb{N}}}
\newcommand{\Z}{\ensuremath{\mathbb{Z}}}
\newcommand{\Q}{\ensuremath{\mathbb{Q}}}
\newcommand{\R}{\ensuremath{\mathbb{R}}}
\newcommand{\C}{\ensuremath{\mathbb{C}}}


\begin{document}
    \title{Nuclear Spaces}
    \author{Nicholas Christoffersen}
    \date{December 8, 2021}
    \maketitle
    
    \section*{Abstract}
    In class, we learned about topological vector spaces, and much about locally convex vector spaces (LCS). However, most of the examples of such spaces came from considering the topological dual of some Banach space. What are examples of LCS that do not come from such a construction? This is the basis for this write up.

    We will consider \textit{Nuclear spaces}, which broadly speaking are the antithesis of Banach spaces in that no infinite dimensional Banach space is also a nuclear space. What are examples of such spaces? We will construct a Nuclear space, and see that an example of such a construction is $C^{\infty}(M)$, the set of smooth functions on a compact manifold $M$.

    \section{Finite-Dimensional TVS}
    We think of Banach spaces as a generalization of finite-dimensional vector spaces (since indeed, the topology on any finite dimensional Hausdorff TVS is the unqiue norm topology). However, in this generalization, we lose a lot of nice properties when moving from the finite-dimensional world to the infinite-dimensional one. For example, the Heine-Borel property: every closed and bounded set in a finite dimensional vector space is compact. Further, when we take the tensor product of a finite dimensional TVS with another TVS, such a tensor product is well-defined. In infinite dimensional TVS, we may not have these properties\cite{Casselman:NuclearSpaces}. However, in the case of nuclear spaces, we do retain these properties.

    \section{Preliminaries}

    \begin{definition}
        Let $J, K$ be seperable Hilbert spaces, and $T: J \to K$ an operator. We say that $T$ is Hilbert-Schmidt if there exists an orthonormal basis $\{j_n\}_{n\in \N}$ such that
        \[
            \sum_{n\in \N}^{} \left\|Tj_n\right\|^2 < \infty.
        \]
    \end{definition}
    \begin{proposition}\label{prop:TACompact}
        Let $T: J \to K$ be a Hilbert-Schmidt operator. Then $T(A)$ is compact in $K$ for any closed, bounded set $A\subseteq J$. In other words, $T$ is a compact operator.
    \end{proposition}

    
    \section{Example}
    Rather than giving the precise definition of a nuclear space, we give a representative example. The interested reader can see \href{https://en.wikipedia.org/wiki/Nuclear_space#Definition}{the Wikipedia entry}\cite{Wiki:NuclearSpaces} for more on the precise definition; there are at least 6 formulations given there!  

    Let $\mathcal{H}$ be a complex topological vector space and assume that there exists collection of increasing Hilbert norms (i.e. norms that come from an inner product) on $\mathcal{H}$ :
    \[
        \left\|\cdot \right\|_0 \leq \left\|\cdot \right\|_1 \leq \ldots
    \]
    Denote $\mathcal{H}_k$ denote the inner product space $(\mathcal{H}, \left<\cdot , \cdot  \right>_k)$ and $H_0$ the completion of $\mathcal{H}_0$. Note then that each $\mathcal{H}_k\subseteq H_0$. Further, if $(x_n)_{n\in \N}$ is a cauchy sequence in $\mathcal{H}_k$, then
    \[
        \left\|x_n - x_m\right\|_{0} \leq \left\|x_n - x_m\right\|_1 \leq \ldots \leq  \left\|x_n - x_m\right\|_{p} \to 0,
    \]
    so that $(x_n)_{n\in \N}$ is a cauchy sequence in $H_0$. Thus we can view the completion of $\mathcal{H}_k$ as sitting inside of $H_0$, which we will denote $H_k$. More generally, the same argument shows that
    \[
        \ldots \subseteq H_k \subseteq H_{k-1} \subseteq \ldots \subseteq H_0.
    \]

    Assume further that the inclusions $\mathcal{H}_{k+1}\hookrightarrow \mathcal{H}_k$ are Hilbert-Schmidt, and we have that
    \[
        \mathcal{H} = \bigcap_{k\in \N} H_k.
    \]
    If $\mathcal{H}$ is endowed with the \textit{projective limit topology}, i.e. the open sets in $\mathcal{H}$ are exactly the unions of $\left\|\cdot \right\|_k$ balls in $\mathcal{H}$, then this makes $\mathcal{H}$ into a \textit{nuclear space}. Note that any choice of a collection of norms satisfying this property gives the same nuclear space -- the specific collection is not necessary to describe the structure, only that there exists such a collection satisfying the above assumptions.

    We note a few things about $\mathcal{H}$ -- it is a LCS, and indeed it is metrizable: If $U$ is an open set in $\mathcal{H}$, then we have that by definition there exists a $\left\|\cdot \right\|_k$ ball, which we will denote $V$, which lies in $U$. Since $V$ is convex, this shows that $\mathcal{H}$ is locally convex. For the second statement, we claim that the following is a metric inducing the same topology on $\mathcal{H}$ :
    \[
        d(x,y) = \sum_{k\in \N}^{} \frac{\left\|x - y\right\|_{k}\wedge 1}{2^{k+1}}.
    \]
    Indeed, let $B(\frac{1}{2^{n}})$ be a ball of radius $\frac{1}{2^{n}}$ about $0$ (using the claimed metric $d$). Then for all $x\in B_n(\frac{1}{2^{n+1}}) := \{x\in \mathcal{H}  \mid \left\|x\right\|_n < 1 / 2^{n+1}\}$, we have
    \begin{align*}
        d(x, 0) 
        &= \sum_{k\in \N}^{} \frac{\left\|x\right\|_k \wedge 1}{2^{k+1}} \\
        &< \sum_{k=0}^{n} \frac{1 / 2^{n+1}}{2^{k+1}} + \sum_{k=n+1}^{\infty} \frac{1}{2^{k+1}}\\
        &\leq \sum_{k\in \N}^{} \frac{1 / 2^{n+1}}{2^{k+1}} + \sum_{k=n+1}^{\infty} \frac{1}{2^{k+1}}\\
        &= \frac{1}{2^{n+1}} + \frac{1}{2^{n+1}} = \frac{1}{2^{n}}
    .\end{align*}
    So that $B_{n}(\frac{1}{2^{n+1}})\subseteq B(\frac{1}{2^{n}})$. For the other inclusion, note that if $x\not\in B_n(r)$, then $x\not\in B(\frac{1\wedge r}{2^{n+1}})$. Therefore, $B(\frac{1\wedge r}{2^{n+1}})\subseteq B_n(r)$, and the two topologies coincide.

    Now, we show a perhaps surprising result:
    \begin{theorem}
        The LCS $\mathcal{H}$ has the Heine-Borel property. That is, every closed and bounded subset $A\subseteq \mathcal{H}$ is compact.
    \end{theorem}
   \begin{proof}
        Let $A\subseteq \mathcal{H}$ be closed and bounded. Since $\mathcal{H}$ is metrizable, it suffices to show that any sequence has a convergent subsequence. To that end, let $x_n$ be an arbitrary sequence in $A$. 

        By the definition of boundedness, for any open neighborhood $U$ of $0$, there exists $r > 0$ such that $A \subseteq rU$. In particular, for each $k\in \N$, there exists an $r_k > 0$ such that
        \[
            A \subseteq r_k B(1) = B_k(r_k) \subseteq \overline{B_k (r_k)},
        \]
        where the closure of each $B_k(r_k)$ occurs inside of $H_k$.
        In particular, $x_n\subseteq B_k(r_k)\subseteq H_k$. However, since the inclusion maps are Hilbert-Schmidt, this implies that $\overline{B_k(r_k)}$ is compact in $H_{k-1}$ for each $k \geq 1$ by Proposition \ref{prop:TACompact}. Therefore, viewing $x_n$ as a sequence in $\iota(\overline{B_1(r_1)})\subseteq H_0$, there exists a convergent subsequence $x^{1}_n$ of $x_n$, and denote $\overline{x} := \lim_{n \to \infty} x^{1}_n$ in $H_0$. Note that it is not necessarily true that $x_n^{1} \to \overline{x}$ in $\mathcal{H}$.

        Since $(x_n)\subseteq A\subseteq \overline{B_2(r_2)}$, we have $(x^{1}_n)\subseteq \iota(\overline{B_2(r_2)})\subseteq H_1$. Again, by compactness, there exists a subsequence $x^{2}_n$ of $x^{1}_n$ such that $x_2^{n}\to \hat{x}\in H_1$ (again, this convergence is happening in $H_1$, not necessarily in $\mathcal{H}$). Further, since limits are unique, the norms are increasing, and a subsequence has the same limit, we get that $\hat{x} = \overline{x}$.

        Inductively, we have for each $k\in \N$, a subsequence $x^{k}_n$ of $x^{k-1}_n$ such that $x^{k}_n \to \overline{x}$ in $H_k$. Now, define $y_n = x^{n}_n$. Then $y_n$ is eventually a subsequence of $x^{k}_n$ for all $k$. In particlar, by definition of the topology on $\mathcal{H}$, we have that $y_n \to \overline{x}\in \bigcap_{k\in \N} H_k = \mathcal{H}$. To see this, fix $0< \epsilon < 1$, and take $N>0$ such that $\frac{1}{2^{N}} < \epsilon / 2$ and $M > 0$ such that $\left\|y_n - \overline{x}\right\|_N < \epsilon / 2$ for all $n > M$. Then we have that for all $n \geq M$,
        \begin{align*}
            d(y_n, y)
            &= \sum_{k=0}^{\infty} \frac{\left\|y_n - y\right\|_k \wedge 1}{2^{k+1}}\\
            &= \sum_{k=0}^{N} \frac{\left\|y_n - y\right\|_k \wedge 1}{2^{k+1}} + \sum_{k=N+1}^{\infty} \frac{\left\|y_n - y\right\|_k \wedge 1}{2^{k+1}}\\
            &\leq \sum_{k=0}^{N} \frac{\epsilon / 2}{2^{k+1}} + \sum_{k=N+1}^{\infty} \frac{1}{2^{k+1}}\\
            &< \epsilon / 2 + \epsilon / 2 = \epsilon
        .\end{align*}
        So indeed, $y_n \to y$ in $\mathcal{H}$.
        %Then since the inclusion maps are Hilbert-Schmidt, this means that there exists a subsequence $x^{1}_n$ of $x_n$ such that $x^{1}_n$ converges in $\iota(B_1(r_1))\subseteq H_0$. Similarly, there exists a further subsequence (i.e. a subsequence of $x^{1}_n$), which we will denote $x^{2}_n$, which converges in $\iota(B_2(r_2))\subseteq H_1\subseteq H_0$. Continuing this process ad infinitum, we get that there exists a subsequence $y_n = x^{n}_n$ such that $y_n\in H_k$ for all $k$, and since subsequences have the same limit as the original sequence, it shows that $y = \lim_{n\in \N}y_n\in \mathcal{H} = \bigcap_{k\in \N} H_k$. However, $A$ is closed, so indeed $y\in A$. This shows that $A$ is compact.
    \end{proof}

    For more facts about the topology of Nuclear spaces, see~\cite{BecnelS2009:NuclearSpaceFacts}.

    \subsection{Concrete Example}
    Notice that this framework perfectly describes $C^{\infty}(M)$, where $M$ is a compact manifold. The increasing sequence of Hilbert norms are given by:
    \[
        \left\|f\right\|_k^2 = \left\|f\right\|_{L^2}^2 + \ldots + \left\|f^{(k)}\right\|_{L^2}^2.
    \]

    For a concrete example, consider the space $C^{\infty}(S^{1})$, where we identify $S^{1}$ with the interval $[0,2\pi]$, identifying the endpoints $0$ and $2\pi$. Recall that
    \begin{align*}
        \int_{0}^{2\pi} \sin(nx)\cos(mx)dx &= 0\\
        \int_{0}^{2\pi} \sin(nx)\sin(mx)dx &= \int_{0}^{2\pi} \cos(nx)\cos(mx) = \delta_{n,m} \pi
    .\end{align*}
    for all integers $n, m$. Using this, it is an exercise to show that the set $\{\sin(nx), \cos(nx)\}_{n = 1}^{\infty}\cup \{1\} $ forms an orthogonal spanning set for every $H_k$ (for each $k$, the completion of $H_k$ in the appropriate norm is called a \textit{Sobolev space}, for more info, see Evans's PDE book~\cite{Evans:PDE}). Further, we have that
    \begin{align*}
        &\left\|\sin(nx)\right\|_k^2 \\
        &= \int_{0}^{2\pi} \sin^2(nx) + n^2\cos^2(nx) + \ldots + n^{2k}
        \left.
        \begin{cases}
            \sin^2(nx) &\text{ if }n\text{ is even}\\
            \cos^2(nx) &\text{ if }n\text{ is odd}\\
    \end{cases} \right\}dx\\
        &= \pi(n^{2k} + n^{2(k-1)} + \ldots + 1)
    ,\end{align*}
    and similarly for $\left\|\cos(nx)\right\|_k^2$. Combining these facts, we see that for each $k\in \N$, the following set is an ONB for $H_k$:
    \[
        \left\{ \frac{\sin(nx)}{\sqrt[]{\pi(n^{2k} + n^{2(k-1)} + \ldots + 1)}}, \frac{\cos(nx)}{\sqrt[]{\pi(n^{2k} + n^{2k-1} + \ldots + 1)}} \right\}_{n = 0}^{\infty} \cup \{1\}.
    \]
    And indeed, we see that the inclusions are Hilbert-Schmidt since 
    \[
        1 + 2\sum_{n=1}^{\infty} \frac{n^{2(k-1)} + n^{2(k-2)} + \ldots + 1}{n^{2k} + n^{2(k-1)} + \ldots + 1} < \infty.
    \]
    The convergence can be seen by comparing the above series in limit to the series $\sum_{n=1}^{\infty} \frac{1}{n^2}$, which is convergent. 
    
    \bibliographystyle{alpha}
    \bibliography{./localbib.bib}
\end{document}
